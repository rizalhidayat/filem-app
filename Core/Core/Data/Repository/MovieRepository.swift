//
//  MovieRepository.swift
//  Movies
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import Combine

public class MovieRepository {
    private let remoteDataSource: MovieRemoteDataSourceProtocol
    private let localDataSource: MovieLocalDataSourceProtocol
    
    public init(remoteDataSource: MovieRemoteDataSourceProtocol = MovieRemoteDataSource.shared, localDataSource: MovieLocalDataSourceProtocol = MovieLocalDataSource.shared) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
    }
    
    public func fetchMovies(keyword: String, page: Int) -> AnyPublisher<[MovieEntity], Error> {
        return remoteDataSource.getMovies(keyword: keyword, page: page)
            .mapError { $0 as Error }
            .flatMap { response -> AnyPublisher<[MovieEntity], Error> in
                self.localDataSource.saveMovies(movies: response)
                return self.localDataSource.loadMovies(keyword: keyword)
                    .mapError { $0 as Error }
                    .eraseToAnyPublisher()
            }
            .catch { _ in
                return self.localDataSource.loadMovies(keyword: keyword)
                    .mapError { $0 as Error }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    public func fetchMovie(id: Int) -> AnyPublisher<MovieEntity, Error> {
        return remoteDataSource.getMovieDetail(id: id)
            .mapError { $0 as Error }
            .flatMap { response -> AnyPublisher<MovieEntity, Error> in
                self.localDataSource.saveMovie(movie: response)
                return self.localDataSource.loadMovie(id: id)
                    .mapError { $0 as Error }
                    .eraseToAnyPublisher()
            }
            .catch { _ in
                return self.localDataSource.loadMovie(id: id)
                    .mapError { $0 as Error }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}
