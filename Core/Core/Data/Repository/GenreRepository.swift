//
//  GenreRepository.swift
//  Movies
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import Combine

public class GenreRepository {
    public static let shared = GenreRepository()
    private let remoteDataSource: GenreRemoteDataSourceProtocol = GenreRemoteDataSource.shared
    private let localDataSource: GenreLocalDataSourceProtocol = GenreLocalDataSource.shared
    
    public func fetchGenres() -> AnyPublisher<[GenreEntity], Error> {
        return remoteDataSource.getGenres()
            .mapError { $0 as Error }
            .flatMap { response -> AnyPublisher<[GenreEntity], Error> in
                self.localDataSource.saveGenres(genres: response)
                return self.localDataSource.loadGenres()
                    .mapError { $0 as Error }
                    .eraseToAnyPublisher()
            }
            .catch { error in
                return self.localDataSource.loadGenres()
                    .mapError { $0 as Error }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}
