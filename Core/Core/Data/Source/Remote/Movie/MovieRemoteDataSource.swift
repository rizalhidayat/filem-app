//
//  MovieRemoteDataSourceProtocol.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import Combine

public protocol MovieRemoteDataSourceProtocol {
    func getMovies(keyword: String, page: Int) -> AnyPublisher<MovieResponse, APIError>
    func getMovieDetail(id: Int) -> AnyPublisher<Movie, APIError>
}

public class MovieRemoteDataSource: MovieRemoteDataSourceProtocol {
    public static let shared = MovieRemoteDataSource()
    private init() { }
    
    public func getMovies(keyword: String, page: Int) -> AnyPublisher<MovieResponse, APIError> {
        let url = URL(string: "https://api.themoviedb.org/3/search/movie")!
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)!
        let queryItems: [URLQueryItem] = [
          URLQueryItem(name: "query", value: keyword),
          URLQueryItem(name: "include_adult", value: "false"),
          URLQueryItem(name: "language", value: "en-US"),
          URLQueryItem(name: "page", value: "\(page)")
        ]
        components.queryItems = components.queryItems.map { $0 + queryItems } ?? queryItems

        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"
        request.timeoutInterval = 10
        request.allHTTPHeaderFields = [
          "accept": "application/json",
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmODE5YWQ0MjM5ZDIwMDhjMzZiOTkwZTlhYWE3ZGRlZiIsInN1YiI6IjVkNTRjZTNmYmYzMWYyMDAxNTM0OGY0ZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.fri1gTwgh1GTIv2xvXYgzFq-2cJjALZa2Fm4WKy3xBo"
        ]
        return APIService.shared.request(request)
    }
    
    public func getMovieDetail(id: Int) -> AnyPublisher<Movie, APIError> {
        let url = URL(string: "https://api.themoviedb.org/3/movie/\(id)")!
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)!
        let queryItems: [URLQueryItem] = [
          URLQueryItem(name: "language", value: "en-US"),
        ]
        components.queryItems = components.queryItems.map { $0 + queryItems } ?? queryItems

        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"
        request.timeoutInterval = 10
        request.allHTTPHeaderFields = [
          "accept": "application/json",
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmODE5YWQ0MjM5ZDIwMDhjMzZiOTkwZTlhYWE3ZGRlZiIsInN1YiI6IjVkNTRjZTNmYmYzMWYyMDAxNTM0OGY0ZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.fri1gTwgh1GTIv2xvXYgzFq-2cJjALZa2Fm4WKy3xBo"
        ]
        return APIService.shared.request(request)
    }
    
}
