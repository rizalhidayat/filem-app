//
//  CastRemoteDataSourceProtocol.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import Combine

public protocol CastRemoteDataSourceProtocol {
    func getCasts(movieId: Int32) -> AnyPublisher<CastResponse, APIError>
}

public class CastRemoteDataSource: CastRemoteDataSourceProtocol {
    public static let shared = CastRemoteDataSource()
    private init() { }
    
    public func getCasts(movieId: Int32) -> AnyPublisher<CastResponse, APIError> {
        
        let url = URL(string: "https://api.themoviedb.org/3/movie/\(movieId)/credits")!
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)!
        let queryItems: [URLQueryItem] = [
          URLQueryItem(name: "language", value: "en-US"),
        ]
        components.queryItems = components.queryItems.map { $0 + queryItems } ?? queryItems

        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"
        request.timeoutInterval = 10
        request.allHTTPHeaderFields = [
          "accept": "application/json",
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmODE5YWQ0MjM5ZDIwMDhjMzZiOTkwZTlhYWE3ZGRlZiIsInN1YiI6IjVkNTRjZTNmYmYzMWYyMDAxNTM0OGY0ZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.fri1gTwgh1GTIv2xvXYgzFq-2cJjALZa2Fm4WKy3xBo"
        ]

        return APIService.shared.request(request)
    }
}
