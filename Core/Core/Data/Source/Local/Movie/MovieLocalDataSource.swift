//
//  MovieLocalDataSource.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import Combine
import CoreData

public protocol MovieLocalDataSourceProtocol {
    func loadMovie(id: Int) -> AnyPublisher<MovieEntity, DBError>
    func loadMovies(keyword: String) -> AnyPublisher<[MovieEntity], DBError>
    func saveMovies(movies: MovieResponse)
    func saveMovie(movie: Movie)
}

public class MovieLocalDataSource: MovieLocalDataSourceProtocol {
    public static let shared = MovieLocalDataSource()
    private let dbService = DBService.shared
    private init() { }
    
    public func loadMovie(id: Int) -> AnyPublisher<MovieEntity, DBError> {
        do {
            let fetchRequest: NSFetchRequest<MovieEntity> = MovieEntity.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id == %ld", id)
            let movies = try dbService.context.fetch(fetchRequest)
            if let movie = movies.first {
                return Just(movie)
                    .setFailureType(to: DBError.self)
                    .eraseToAnyPublisher()
            } else {
                return Fail(error: DBError.failedToInitializeManagedObjectModel)
                    .eraseToAnyPublisher()
            }
        } catch {
            return Fail(error: DBError.unresolvedError(error as NSError))
                .eraseToAnyPublisher()
        }
    }
    
    public func loadMovies(keyword: String) -> AnyPublisher<[MovieEntity], DBError> {
        do {
            let fetchRequest: NSFetchRequest<MovieEntity> = MovieEntity.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "title CONTAINS[cd] %@", keyword)
            let movies = try dbService.context.fetch(fetchRequest)
            return Just(movies)
                .setFailureType(to: DBError.self)
                .eraseToAnyPublisher()
        } catch {
            return Fail(error: DBError.unresolvedError(error as NSError))
                .eraseToAnyPublisher()
        }
    }
    
    public func saveMovies(movies: MovieResponse) {
        guard let movies = movies.results else { return }
        for movie in movies {
            saveMovie(movie: movie)
        }
    }
    
    public func saveMovie(movie: Movie) {
        guard let id = movie.id else {
            return
        }

        dbService.context.performAndWait {
            do {
                let fetchRequest: NSFetchRequest<MovieEntity> = MovieEntity.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "id == %ld", id)

                let existingMovies = try dbService.context.fetch(fetchRequest)

                let movieEntity: MovieEntity

                if let existingMovie = existingMovies.first {
                    movieEntity = existingMovie
                } else {
                    movieEntity = MovieEntity(context: dbService.context)
                    movieEntity.id = Int32(id)
                }

                updateMovieEntity(movieEntity: movieEntity, with: movie)

                try dbService.saveContext()
            } catch {
                // Handle error
                print("Error: \(error)")
            }
        }
    }
    
    //MARK: - Helper
    private func updateMovieEntity(movieEntity: MovieEntity, with movie: Movie) {
        movieEntity.title = movie.title
        movieEntity.duration = movie.runtime ?? 0
        movieEntity.movieDescription = movie.overview
        movieEntity.posterPath = movie.posterPath
        movieEntity.backgroundDrop = movie.backdropPath
        movieEntity.releaseDate = movie.releaseDate
        if let genreIds = movie.genreIDS {
            let genres = genreIds.compactMap { loadGenre(id: $0) }
            movieEntity.genres = NSSet(array: genres)
        }
        
        if let genres = movie.genres {
            let genresEntities = genres.compactMap { genre in
                if let id = genre.id {
                    return loadGenre(id: id)
                } else {
                    return nil
                }
            }
            movieEntity.genres = NSSet(array: genresEntities)
        }
    }
    
    private func loadGenre(id: Int32) -> GenreEntity? {
        var genreEntity: GenreEntity?

        dbService.context.performAndWait {
            do {
                let fetchRequest: NSFetchRequest<GenreEntity> = GenreEntity.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "id == %ld", id)
                let genres = try dbService.context.fetch(fetchRequest)
                genreEntity = genres.first
            } catch {
                print("Error fetching genre: \(error)")
            }
        }

        return genreEntity
    }

    
}
