//
//  GenreLocalDataSource.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import CoreData
import Combine

public protocol GenreLocalDataSourceProtocol {
    func saveGenres(genres: GenreResponse)
    func loadGenres() -> AnyPublisher<[GenreEntity], DBError>
}

public class GenreLocalDataSource: GenreLocalDataSourceProtocol {
    public static let shared = GenreLocalDataSource()
    private let dbService = DBService.shared
    private init() { }
    
    public func saveGenres(genres: GenreResponse) {
        guard let genres = genres.genres else {
            print("Genre is nil")
            return
        }

        for genre in genres {
            guard let id = genre.id else {
                continue
            }
            do {
                let fetchRequest: NSFetchRequest<GenreEntity> = GenreEntity.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "id == %ld", id)
                
                let existingGenres = try dbService.context.fetch(fetchRequest)
                
                if let existingGenre = existingGenres.first {
                    existingGenre.name = genre.name
                    // Update other properties as needed
                } else {
                    let newGenreEntity = GenreEntity(context: dbService.context)
                    newGenreEntity.id = Int32(id)
                    newGenreEntity.name = genre.name
                    // Set other properties as needed
                }
                
                try dbService.saveContext()
            } catch {
                print("Error saving genre: \(error)")
            }
        }
    }
    
    public func loadGenres() -> AnyPublisher<[GenreEntity], DBError> {
        do {
            let fetchRequest: NSFetchRequest<GenreEntity> = GenreEntity.fetchRequest()
            let genres = try dbService.context.fetch(fetchRequest)
            return Just(genres).setFailureType(to: DBError.self).eraseToAnyPublisher()
        } catch {
            return Fail(error: DBError.unresolvedError(error as NSError)).eraseToAnyPublisher()
        }
    }
}
