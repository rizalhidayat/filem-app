//
//  CastLocalDataSource.swift
//  Core
//
//  Created by Rizal Hidayat on 25/04/24.
//

import Foundation
import Combine
import CoreData

public protocol CastLocalDataSourceProtocol {
    func loadCasts(movieId: Int32) -> AnyPublisher<[CastEntity], DBError>
    func saveCasts(casts: CastResponse)
}

public class CastLocalDataSource: CastLocalDataSourceProtocol {
    public static let shared = CastLocalDataSource()
    private let dbService = DBService.shared
    private init() { }
    
    public func loadCasts(movieId: Int32) -> AnyPublisher<[CastEntity], DBError> {
        return Future<[CastEntity], DBError> { promise in
            do {
                let fetchRequest: NSFetchRequest<MovieEntity> = MovieEntity.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "id == %ld", movieId)
                let movies = try self.dbService.context.fetch(fetchRequest)
                
                if let movieEntity = movies.first, let casts = movieEntity.casts?.allObjects as? [CastEntity] {
                    promise(.success(casts))
                } else {
                    promise(.success([]))
                }
            } catch {
                promise(.failure(.failedToLocateDataModel))
            }
        }
        .eraseToAnyPublisher()
    }

    
    public func saveCasts(casts: CastResponse) {
        guard let movieId = casts.id, let casts = casts.cast else { return }
        var castEntities: [CastEntity] = []
        for cast in casts {
            if let newEntity = saveCast(movieId: movieId, cast: cast) {
                castEntities.append(newEntity)
            }
        }
        saveCastsToMovie(movieId: movieId, casts: castEntities)
    }
    
    private func saveCast(movieId: Int32, cast: Cast) -> CastEntity? {
        guard let id = cast.id else {
            return nil
        }
        
        var castEntity: CastEntity?
        
        dbService.context.performAndWait {
            do {
                let fetchRequest: NSFetchRequest<CastEntity> = CastEntity.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "id == %@", NSNumber(value: id))
                let existingCasts = try dbService.context.fetch(fetchRequest)
                
                if let existingCast = existingCasts.first {
                    existingCast.name = cast.name
                    existingCast.photoURL = cast.profilePath
                    castEntity = existingCast
                } else {
                    castEntity = CastEntity(context: dbService.context)
                    castEntity?.id = id
                    castEntity?.name = cast.name
                    castEntity?.photoURL = cast.profilePath
                }
                
                try dbService.saveContext()
            } catch {
                print("Error: \(error)")
            }
        }
        
        return castEntity
    }
    
    //MARK: - Helpers
    private func saveCastsToMovie(movieId: Int32, casts: [CastEntity]) {
        dbService.context.performAndWait {
            do {
                let fetchRequest: NSFetchRequest<MovieEntity> = MovieEntity.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "id == %ld", movieId)
                let movies = try dbService.context.fetch(fetchRequest)
                if let movieEntity = movies.first {
                    movieEntity.casts = NSSet(array: casts)
                }
            } catch {
                print("Error fetching genre: \(error)")
            }
        }
    }
}
