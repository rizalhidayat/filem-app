//
//  CastRepository.swift
//  Core
//
//  Created by Rizal Hidayat on 25/04/24.
//

import Foundation
import Combine

public class CastRepository {
    private let remoteDataSource: CastRemoteDataSourceProtocol
    private let localDataSource: CastLocalDataSourceProtocol
    
    public init(remoteDataSource: CastRemoteDataSourceProtocol = CastRemoteDataSource.shared, localDataSource: CastLocalDataSourceProtocol = CastLocalDataSource.shared) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
    }
    
    public func fetchCasts(movieId: Int32) -> AnyPublisher<[CastEntity], Error> {
        return remoteDataSource.getCasts(movieId: movieId)
            .mapError { $0 as Error }
            .flatMap { response -> AnyPublisher<[CastEntity], Error> in
                self.localDataSource.saveCasts(casts: response)
                return self.localDataSource.loadCasts(movieId: movieId)
                    .mapError { $0 as Error }
                    .eraseToAnyPublisher()
            }
            .catch { _ in
                return self.localDataSource.loadCasts(movieId: movieId)
                    .mapError { $0 as Error }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
        
    }
}
