//
//  MovieEntity+CoreDataProperties.swift
//  
//
//  Created by Rizal Hidayat on 24/04/24.
//
//

import Foundation
import CoreData


extension MovieEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MovieEntity> {
        return NSFetchRequest<MovieEntity>(entityName: "MovieEntity")
    }

    @NSManaged public var id: Int32
    @NSManaged public var title: String?
    @NSManaged public var releaseDate: String?
    @NSManaged public var duration: Int32
    @NSManaged public var movieDescription: String?
    @NSManaged public var posterPath: String?
    @NSManaged public var backgroundDrop: String?
    @NSManaged public var genres: NSSet?
    @NSManaged public var casts: NSSet?

}

// MARK: Generated accessors for genres
extension MovieEntity {

    @objc(addGenresObject:)
    @NSManaged public func addToGenres(_ value: GenreEntity)

    @objc(removeGenresObject:)
    @NSManaged public func removeFromGenres(_ value: GenreEntity)

    @objc(addGenres:)
    @NSManaged public func addToGenres(_ values: NSSet)

    @objc(removeGenres:)
    @NSManaged public func removeFromGenres(_ values: NSSet)

}

// MARK: Generated accessors for casts
extension MovieEntity {

    @objc(addCastsObject:)
    @NSManaged public func addToCasts(_ value: CastEntity)

    @objc(removeCastsObject:)
    @NSManaged public func removeFromCasts(_ value: CastEntity)

    @objc(addCasts:)
    @NSManaged public func addToCasts(_ values: NSSet)

    @objc(removeCasts:)
    @NSManaged public func removeFromCasts(_ values: NSSet)

}
