//
//  Genre.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation

// MARK: - Genre
struct Genre: Decodable {
    let id: Int32?
    let name: String?
}
