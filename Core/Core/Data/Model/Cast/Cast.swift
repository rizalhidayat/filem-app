//
//  Cast.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation

// MARK: - Cast
public struct Cast: Decodable {
    let id: Int32?
    let name: String?
    let profilePath: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case profilePath = "profile_path"
    }
}
