//
//  CastEntity+CoreDataProperties.swift
//  
//
//  Created by Rizal Hidayat on 24/04/24.
//
//

import Foundation
import CoreData


extension CastEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CastEntity> {
        return NSFetchRequest<CastEntity>(entityName: "CastEntity")
    }

    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var photoURL: String?
    @NSManaged public var movies: MovieEntity?

}
