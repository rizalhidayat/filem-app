//
//  MovieResponse.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation

// MARK: - MovieResponse
public struct MovieResponse: Decodable {
    let page: Int?
    let results: [Movie]?
    let totalPages: Int?
    let totalResults: Int?

    enum CodingKeys: String, CodingKey {
        case page = "page"
        case results = "results"
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

// MARK: - Result
public struct Movie: Decodable {
    let backdropPath: String?
    let genreIDS: [Int32]?
    let genres: [Genre]?
    let id: Int32?
    let overview: String?
    let posterPath: String?
    let releaseDate: String?
    let title: String?
    let runtime: Int32?

    enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case genreIDS = "genre_ids"
        case genres = "genres"
        case id = "id"
        case overview = "overview"
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title = "title"
        case runtime = "runtime"
    }
}
