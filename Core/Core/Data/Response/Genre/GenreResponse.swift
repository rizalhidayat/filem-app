//
//  GenreResponse.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation

public struct GenreResponse: Decodable {
    let genres: [Genre]?
}

