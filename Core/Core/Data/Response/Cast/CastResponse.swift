//
//  CastResponse.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation

// MARK: - CastResponse
public struct CastResponse: Decodable {
    let id: Int32?
    let cast: [Cast]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case cast = "cast"
    }
}
