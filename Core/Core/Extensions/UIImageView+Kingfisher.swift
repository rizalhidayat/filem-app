//
//  UIImageView+Kingfisher.swift
//  Core
//
//  Created by Rizal Hidayat on 25/04/24.
//

import UIKit
import Kingfisher

extension UIImageView {
    public func setLowResImage(path: String) {
        self.kf.indicatorType = .activity
        let smallImageUrl = "https://image.tmdb.org/t/p/w154"
        let urlString = smallImageUrl + path
        if let url = URL(string: urlString) {
            self.kf.setImage(with: url)
        }
    }
    
    public func setOriginalResImage(path: String) {
        self.kf.indicatorType = .activity
        let originalImageUrl = "https://image.tmdb.org/t/p/original"
        let urlString = originalImageUrl + path
        if let url = URL(string: urlString) {
            self.kf.setImage(with: url)
        }
    }
}
