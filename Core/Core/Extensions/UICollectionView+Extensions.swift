//
//  UICollectionView+Extensions.swift
//  Core
//
//  Created by Rizal Hidayat on 25/04/24.
//

import UIKit

extension UICollectionView {
    public func register<T: UICollectionViewCell>(cellClass: T.Type) {
        register(UINib(nibName: cellClass.reuseIdentifier, bundle: Bundle(for: T.self)), forCellWithReuseIdentifier: cellClass.reuseIdentifier)
    }
    
    public func dequeue<T: UICollectionViewCell>(cellClass: T.Type, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(
            withReuseIdentifier: cellClass.reuseIdentifier, for: indexPath) as? T else {
            fatalError(
                "Error: cell with id: \(cellClass.reuseIdentifier) for indexPath: \(indexPath) is not \(T.self)")
        }
        return cell
    }
}


extension UICollectionViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

