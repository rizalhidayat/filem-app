//
//  ApiService.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import Combine
import Network

public enum APIError: Error {
    case invalidURL
    case networkError(Error)
    case invalidResponse
    case noInternetConnection
}

class APIService {
    static let shared = APIService()
    private let monitor = NWPathMonitor()
    private let queue = DispatchQueue(label: "NetworkMonitor")

    private init() {
        startMonitoringNetwork()
    }

    private func startMonitoringNetwork() {
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("Internet connection is available")
            } else {
                print("No internet connection")
            }
        }

        monitor.start(queue: queue)
    }

    func request<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, APIError> {
        guard monitor.currentPath.status == .satisfied else {
            return Fail(error: APIError.noInternetConnection)
                .eraseToAnyPublisher()
        }
        return URLSession.shared.dataTaskPublisher(for: request)
            .mapError { error in
                APIError.networkError(error)
            }
            .flatMap { data, response -> AnyPublisher<T, APIError> in
                guard let httpResponse = response as? HTTPURLResponse, 200..<300 ~= httpResponse.statusCode else {
                    return Fail(error: APIError.invalidResponse).eraseToAnyPublisher()
                }

                return Just(data)
                    .decode(type: T.self, decoder: JSONDecoder())
                    .mapError { _ in APIError.invalidResponse }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}
