//
//  DBService.swift
//  Core
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import CoreData

public enum DBError: Error {
    case failedToLocateDataModel
    case failedToInitializeManagedObjectModel
    case unresolvedError(NSError)
}

public class DBService {
    public static let shared = DBService()
    
    private init() {}
    
    lazy var persistentContainer: NSPersistentContainer = {
        guard let modelURL = Bundle(identifier: "Rizal-Hidayat.Core")?.url(forResource: "Filem", withExtension: "momd") else {
            fatalError("Failed to locate data model file in specified bundle.")
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Failed to initialize managed object model from: \(modelURL)")
        }
        
        let container = NSPersistentContainer(name: "Payment", managedObjectModel: managedObjectModel)
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    func saveContext() throws {
        guard context.hasChanges else { return }
        
        do {
            try context.save()
        } catch {
            throw DBError.unresolvedError(error as NSError)
        }
    }
}

