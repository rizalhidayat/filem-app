//
//  Coordinator.swift
//  FilemApp
//
//  Created by Rizal Hidayat on 25/04/24.
//

import UIKit
import Movies
import MovieDetail

class Coordinator: NSObject {
    private let window: UIWindow
    private let movieMainVM = MovieMainViewModel()

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let movieMainVC = MovieMainViewController(viewModel: movieMainVM)
        movieMainVC.routeToDetailCallback = { [weak self] movieId in
            self?.showMovieDetail(movieId: movieId)
        }
        let navigationController = UINavigationController(rootViewController: movieMainVC)
        navigationController.delegate = self
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    private func showMovieDetail(movieId: Int32) {
        let detailVM = MovieDetailViewModel(movieId: movieId)
        let detailVC = MovieDetailViewController(viewModel: detailVM)
        if let rootViewController = window.rootViewController as? UINavigationController {
            rootViewController.pushViewController(detailVC, animated: true)
        }
    }
}


extension Coordinator: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if #available(iOS 14.0, *) {
            viewController.navigationItem.backButtonDisplayMode = .minimal
        } else {
            viewController.navigationItem.backButtonTitle = ""
        }
    }
}
