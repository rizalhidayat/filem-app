//
//  SceneDelegate.swift
//  FilemApp
//
//  Created by Rizal Hidayat on 23/04/24.
//

import UIKit
import Movies
import MovieDetail

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var coordinator: Coordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let mainWindow = UIWindow(windowScene: windowScene)
        window = mainWindow
        coordinator = Coordinator(window: mainWindow)
        coordinator?.start()
    }
}
