//
//  MovieDetailViewModel.swift
//  MovieDetail
//
//  Created by Rizal Hidayat on 25/04/24.
//

import Core
import Foundation
import Combine

public class MovieDetailViewModel {
    private let movieRepository = MovieRepository()
    private let castRepository = CastRepository()
    private var cancellables = Set<AnyCancellable>()
    private let movieId: Int32
    
    @Published private(set) var movie: MovieEntity?
    @Published private(set) var casts: [CastEntity] = []
    @Published private(set) var errorMessage: String?
    
    public init(movieId: Int32) {
        self.movieId = movieId
        fetchMovie()
        fetchCasts()
    }
    
    private func fetchMovie() {
        movieRepository.fetchMovie(id: Int(movieId)).receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
            case .finished:
                break
            case .failure(let error):
                self.errorMessage = error.localizedDescription
            }
            } receiveValue: { movie in
                self.movie = movie
            }.store(in: &cancellables)
    }
    
    private func fetchCasts() {
        castRepository.fetchCasts(movieId: movieId)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                }
            } receiveValue: { casts in
                self.casts = casts
            }.store(in: &cancellables)

    }
}
