//
//  TitleCollectionReusableView.swift
//  MovieDetail
//
//  Created by Rizal Hidayat on 26/04/24.
//

import UIKit

class TitleCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var titleLabel: UILabel!
  
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureLabel(text: String) {
        titleLabel.text = text
    }
}
