//
//  MovieCastCollectionViewCell.swift
//  MovieDetail
//
//  Created by Rizal Hidayat on 25/04/24.
//

import UIKit
import Core

class MovieCastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        photoImageView.layer.cornerRadius = photoImageView.bounds.width / 2
        photoImageView.layer.masksToBounds = true
    }
    
    func configure(with cast: CastEntity) {
        nameLabel.text = cast.name
        if let url = cast.photoURL {
            photoImageView.setOriginalResImage(path: url)
        }
    }
}
