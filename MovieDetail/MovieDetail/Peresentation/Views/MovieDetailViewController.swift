//
//  MovieDetailViewController.swift
//  MovieDetail
//
//  Created by Rizal Hidayat on 25/04/24.
//

import UIKit
import Combine
import Core

public class MovieDetailViewController: UIViewController {
    
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var castCollectionView: UICollectionView!
  
    private lazy var dataSource = makeDataSource()
    
    private let viewModel: MovieDetailViewModel
    private var cancellables = Set<AnyCancellable>()
    
    public init(viewModel: MovieDetailViewModel) {
        let bundle = Bundle(for: MovieDetailViewController.self)
        let nibName = String(describing: MovieDetailViewController.self)
        self.viewModel = viewModel
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not supported!")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        initNavigationBar()
        initCollectionView()
        initSubscribers()
    }
    
    private func initNavigationBar() {
        title = "Movie Detail"
    }
    
    private func initCollectionView() {
        castCollectionView.register(cellClass: MovieCastCollectionViewCell.self)
        castCollectionView.register(TitleCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "TitleCollectionReusableView")
        castCollectionView.register(UINib(nibName: "TitleCollectionReusableView", bundle: Bundle(for: TitleCollectionReusableView.self)), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "TitleCollectionReusableView")
        castCollectionView.dataSource = dataSource
        castCollectionView.collectionViewLayout = createLayout()
        
    }
    
    func createLayout() -> UICollectionViewLayout {
           let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.3),
                                                  heightDimension: .fractionalHeight(1))
           let item = NSCollectionLayoutItem(layoutSize: itemSize)
           
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                  heightDimension: .fractionalHeight(0.7))
           let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, repeatingSubitem: item, count: 3)
           group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 0)
           
           let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .groupPagingCentered
           let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                   heightDimension: .absolute(50))
           let header = NSCollectionLayoutBoundarySupplementaryItem(
               layoutSize: headerSize,
               elementKind: UICollectionView.elementKindSectionHeader,
               alignment: .top)
           
           section.boundarySupplementaryItems = [header]
           
           let layout = UICollectionViewCompositionalLayout(section: section)
        
           return layout
       }
    
    private func initSubscribers() {
        viewModel.$movie.receive(on: DispatchQueue.main)
            .compactMap { $0 }
            .sink { [weak self] movie in
                self?.updateInformation(with: movie)
            }.store(in: &cancellables)
        viewModel.$casts.receive(on: DispatchQueue.main)
            .compactMap { $0 }
            .sink { [weak self] casts in
                self?.update(with: casts)
            }.store(in: &cancellables)
    }
    
    private func updateInformation(with movie: MovieEntity) {
        if let path = movie.backgroundDrop {
            backdropImageView.setOriginalResImage(path: path)
        }
        titleLabel.text = movie.title
        durationLabel.text = formatDuration(minutes: movie.duration)
        overviewLabel.text = movie.movieDescription
        // Format genres as comma-separated list
        if let genres = movie.genres {
            guard let genresSet = genres as? Set<GenreEntity>, !genresSet.isEmpty else {
                genreLabel.text = "N/A"
                return }
            let genreNames = genresSet.compactMap { $0.name }
            genreLabel.text = genreNames.joined(separator: ", ")
        } else {
            genreLabel.text = "N/A"
        }
    }
    
    private func formatDuration(minutes: Int32) -> String {
        let hours = minutes / 60
        let remainingMinutes = minutes % 60
        return "\(hours)h \(remainingMinutes)m"
    }
    
}

extension MovieDetailViewController {
    private enum Section: String, CaseIterable {
        case casts = "Casts"
    }
    
    private func makeDataSource() -> UICollectionViewDiffableDataSource<Section, CastEntity> {
        let dataSource: UICollectionViewDiffableDataSource<Section, CastEntity> = .init(collectionView: castCollectionView) { collectionView, indexPath, cast in
            let cell = collectionView.dequeue(cellClass: MovieCastCollectionViewCell.self, forIndexPath: indexPath)
            cell.configure(with: cast)
            return cell
        }
       
        dataSource.supplementaryViewProvider = { collectionView, kind, indexPath in
            guard let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TitleCollectionReusableView", for: indexPath) as? TitleCollectionReusableView else { fatalError("Unable to dequeu") }
            
            let section = dataSource.snapshot().sectionIdentifiers[indexPath.section]
            sectionHeader.configureLabel(text: section.rawValue)
            return sectionHeader
        }
        return dataSource
    }
    
    private func update(with casts: [CastEntity], animate: Bool = true) {
        DispatchQueue.main.async {
            var snapshot = NSDiffableDataSourceSnapshot<Section, CastEntity>()
            snapshot.appendSections(Section.allCases)
            snapshot.appendItems(casts, toSection: .casts)
            self.dataSource.apply(snapshot, animatingDifferences: animate)
        }
    }
}

