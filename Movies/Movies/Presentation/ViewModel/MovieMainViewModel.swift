//
//  MovieMainViewModel.swift
//  Movies
//
//  Created by Rizal Hidayat on 24/04/24.
//

import Foundation
import Combine
import Core

public class MovieMainViewModel {
    private let movieRepository = MovieRepository()
    
    @Published private(set) var movies: [MovieEntity] = []
    @Published private(set) var errorMessage: String?
    
    private var cancellables = Set<AnyCancellable>()
    private var page = 1
    private var isLastPage = false
    
    public init() { }
    
    func searchMovie(keyword: String) {
        page = 1
        isLastPage = false
        if keyword.isEmpty {
            movies.removeAll()
            return
        }
        movieRepository.fetchMovies(keyword: keyword, page: 1).receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                }
            } receiveValue: { movies in
                self.movies = movies
            }.store(in: &cancellables)
    }
    
    func loadNextPage(keyword: String) {
        page += 1
        if isLastPage {
            return
        }
        movieRepository.fetchMovies(keyword: keyword, page: page).receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.isLastPage = true
                    self.errorMessage = error.localizedDescription
                }
            } receiveValue: { movies in
                if movies.isEmpty {
                    self.isLastPage = true
                    return
                }
                self.movies = movies
            }.store(in: &cancellables)
    }

}
