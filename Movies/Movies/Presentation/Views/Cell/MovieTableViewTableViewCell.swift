//
//  MovieTableViewTableViewCell.swift
//  Movies
//
//  Created by Rizal Hidayat on 23/04/24.
//

import UIKit
import Core

class MovieTableViewTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with movie: MovieEntity) {
        titleLabel.text = movie.title
        if let posterPath = movie.posterPath {
            posterImageView.setLowResImage(path: posterPath)
        }
        // Format releaseDate to display only the year
        if let releaseDate = movie.releaseDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            if let date = formatter.date(from: releaseDate) {
                formatter.dateFormat = "yyyy"
                yearLabel.text = formatter.string(from: date)
            } else {
                yearLabel.text = "N/A"
            }
        } else {
            yearLabel.text = "N/A"
        }
        
        // Format genres as comma-separated list
        if let genres = movie.genres {
            guard let genresSet = genres as? Set<GenreEntity>, !genresSet.isEmpty else { 
                genreLabel.text = "N/A"
                return }
                    let genreNames = genresSet.compactMap { $0.name }
            genreLabel.text = genreNames.joined(separator: ", ")
        } else {
            genreLabel.text = "N/A"
        }
    }

    
}
