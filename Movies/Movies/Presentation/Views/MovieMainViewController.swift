//
//  MovieMainViewController.swift
//  Movies
//
//  Created by Rizal Hidayat on 23/04/24.
//

import UIKit
import Combine
import Core

public class MovieMainViewController: UIViewController {
    
    @IBOutlet weak var movieTableView: UITableView!
    private var keyword = PassthroughSubject<String, Never>()
    private lazy var dataSource = makeDataSource()
    private var cancellables = Set<AnyCancellable>()
    private let viewModel: MovieMainViewModel
    public var routeToDetailCallback: ((Int32) -> Void)!
    
    private let refreshControl = UIRefreshControl()
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        return searchController
    }()
    
    public init(viewModel: MovieMainViewModel) {
        let bundle = Bundle(for: MovieMainViewController.self)
        let nibName = String(describing: MovieMainViewController.self)
        self.viewModel = viewModel
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not supported!")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        initNavigationBar()
        initTableView()
        initSubscribers()
    }
    
    private func initNavigationBar() {
        definesPresentationContext = true
        title = "Movies"
        navigationItem.searchController = searchController
    }
    
    private func initTableView() {
        movieTableView.isUserInteractionEnabled = false
        movieTableView.refreshControl = refreshControl
        movieTableView.register(cellClass: MovieTableViewTableViewCell.self)
        movieTableView.dataSource = dataSource
        movieTableView.delegate = self
        refreshControl.addTarget(self, action: #selector(refreshSearch), for: .valueChanged)
    }
    
    private func initSubscribers() {
        keyword.receive(on: DispatchQueue.main)
            .dropFirst()
            .debounce(for: .seconds(0.5), scheduler: DispatchQueue.main)
            .sink { [weak self] searchText in
                self?.movieTableView.isUserInteractionEnabled = !searchText.isEmpty
                self?.viewModel.searchMovie(keyword: searchText)
            }
            .store(in: &cancellables)
        viewModel.$movies.receive(on: DispatchQueue.main)
            .dropFirst()
            .sink { [weak self] movies in
                self?.refreshControl.endRefreshing()
                self?.update(with: movies)
            }
            .store(in: &cancellables)
    }
    
    @objc
    private func refreshSearch() {
        if let keyword = searchController.searchBar.text {
            refreshControl.beginRefreshing()
            viewModel.searchMovie(keyword: keyword)
        }
    }
}

//MARK: Search bar
extension MovieMainViewController: UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        keyword.send(searchText)
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        keyword.send("")
    }
}

//MARK: TableView
extension MovieMainViewController {
    private enum Section: CaseIterable {
        case movies
    }
    
    private func makeDataSource() -> UITableViewDiffableDataSource<Section, MovieEntity> {
        return UITableViewDiffableDataSource(tableView: movieTableView) { tableView, indexPath, movie in
            let cell = tableView.dequeue(cellClass: MovieTableViewTableViewCell.self, forIndexPath: indexPath)
            cell.configure(with: movie)
            return cell
        }
    }
    
    private func update(with movies: [MovieEntity], animate: Bool = true) {
        DispatchQueue.main.async {
            var snapshot = NSDiffableDataSourceSnapshot<Section, MovieEntity>()
            snapshot.appendSections(Section.allCases)
            snapshot.appendItems(movies, toSection: .movies)
            self.dataSource.apply(snapshot, animatingDifferences: animate)
        }
    }
}


extension MovieMainViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let snapshot = dataSource.snapshot()
        let selectedMovieId = snapshot.itemIdentifiers[indexPath.row].id
        routeToDetailCallback(selectedMovieId)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchController.searchBar.resignFirstResponder()
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.movies.count - 1,
           let keyword = searchController.searchBar.text{
            viewModel.loadNextPage(keyword: keyword)
        }
    }
}
